## Elevator task
Simple program that simulates working elevator in building with n floors, 
where 5 <= n <= 20.On each floor k passengers where  0 <= k <= 10.

In this task I provide to variants of solving.One with functions, and one with classes.

## Usage

To run example with functions
```commandline
python3 main_with_functions.py
```
To run example with classes
```commandline
python3 main_with_class.py
```
By default time delay is 1 second between frames, but you can set it before run like this.
```commandline
python main_with_class.py -d 5
```
