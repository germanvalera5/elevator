import random
import time
import argparse
from collections import deque


class Building:

    def __init__(self):
        self.building = []

    def create_floors_with_passengers(self):
        building_height = random.randint(5, 20)

        for floor in range(building_height + 1):
            people = random.randint(0, 10)
            other_floors = [i for i in range(building_height) if i != floor]
            self.building.append([random.choice(other_floors) for _ in range(people)])

    def __repr__(self):
        return 'Building with {} floors'.format(len(self.building))

    def __iter__(self):
        return (i for i in self.building)

    def __getitem__(self, item):
        return self.building[item]

    def __len__(self):
        return len(self.building)


class Elevator:

    def __init__(self, building: Building):
        self.building = building
        self.elevator = deque([], maxlen=5)
        self.destination_floor = None
        self.moving_up = True
        self.current_floor = 0
        self.count = 0

    def load(self, passenger):
        self.elevator.append(passenger)

    def unload(self, passenger):
        self.elevator.remove(passenger)

    def is_full(self):
        return len(self.elevator) == 5

    def load_unload_passengers(self):
        n = len(self.building)
        m = len(self.building[self.current_floor])
        other_floors = [i for i in range(n) if i != self.current_floor]
        unload_passengers = 0

        if self.current_floor in elevator:

            for _ in range(self.elevator.count(self.current_floor)):
                self.elevator.remove(self.current_floor)
                building[self.current_floor].insert(0, random.choice(other_floors))
                unload_passengers += 1
            for i in building[self.current_floor][unload_passengers:]:
                if self.is_full():
                    break
                if self.moving_up:
                    if i > self.current_floor:
                        building[self.current_floor].remove(i)
                        self.load(i)
                else:
                    if i < self.current_floor:
                        building[self.current_floor].remove(i)
                        self.load(i)

        elif not self.is_full():

            for _ in range(m):

                if self.is_full():
                    break

                passenger = building[self.current_floor].pop()

                if self.moving_up:
                    if passenger > self.current_floor:
                        self.load(passenger)
                    else:
                        building[self.current_floor].insert(0, passenger)
                else:
                    if passenger < self.current_floor:
                        self.load(passenger)
                    else:
                        building[self.current_floor].insert(0, passenger)

        else:

            return self.elevator, self.destination_floor

        if self.elevator:

            if self.moving_up:

                max_in_elevator = max(elevator)
                if self.destination_floor is None:
                    self.destination_floor = max_in_elevator
                if max_in_elevator > self.destination_floor:
                    self.destination_floor = max_in_elevator
            else:
                min_in_elevator = min(elevator)
                if self.destination_floor is None:
                    self.destination_floor = min_in_elevator
                if min_in_elevator < self.destination_floor:
                    self.destination_floor = min_in_elevator

        return self.elevator, self.destination_floor

    def show_current_situation(self, sleep_time=None):
        print('{:-^50}'.format(f' stage {self.count} '))
        print('{:5} | {:^30} | {:2}'.format('floor', 'elevator', 'passengers on each floor'))
        for idx, floor in reversed(list(enumerate(self.building))):
            passengers = ' '.join(str(i) for i in floor)
            lift = ' '.join(str(i) for i in self.elevator) if self.elevator else 'empty'
            if self.current_floor == idx:
                msg = '{:5} | {:^30} ^| {:2}'
                print(msg.format(idx, lift, passengers))
            else:
                msg = '{:5} | {:^30} | {:2}'
                print(msg.format(idx, '', passengers))
        print('{:*^50}'.format(''))
        if sleep_time:
            time.sleep(sleep_time)

    def decide_move_up_or_down(self):
        down = 0
        up = 0
        for passenger in building[self.current_floor]:
            if passenger > self.current_floor:
                up += 1
            else:
                down += 1
        if up == down:
            return bool(random.choice([down, up]))
        return up > down

    def did_we_arrive(self):
        if self.current_floor == self.destination_floor and len(elevator) == 0:
            if self.moving_up:
                self.current_floor -= 1
            else:
                self.current_floor += 1
            self.moving_up = None
            self.destination_floor = None
            return self.current_floor, self.moving_up, self.destination_floor
        return self.current_floor, self.moving_up, self.destination_floor

    def run(self, sleep_time=None):
        while True:
            self.show_current_situation(sleep_time)
            if self.moving_up is None:
                self.moving_up = self.decide_move_up_or_down()
            if self.moving_up:
                self.elevator, self.destination_floor = self.load_unload_passengers()
                self.current_floor, self.moving_up, self.destination_floor = self.did_we_arrive()
                self.current_floor += 1
            else:
                self.elevator, self.destination_floor = self.load_unload_passengers()
                self.current_floor, self.moving_up, self.destination_floor = self.did_we_arrive()
                self.current_floor -= 1

            self.count += 1

    def __iter__(self):
        return (i for i in self.elevator)

    def __repr__(self):
        return 'Elevator with {} passengers'.format(len(self.elevator))

    def __len__(self):
        return len(self.elevator)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Elevator Simulation')
    parser.add_argument(
        '-d', '--delay', type=int, default=1,
        help='Time delay in seconds.(default=1)'
    )
    args = parser.parse_args()
    building = Building()
    building.create_floors_with_passengers()
    elevator = Elevator(building)
    elevator.run(sleep_time=args.delay)
