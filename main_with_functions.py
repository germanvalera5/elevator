import argparse
import random
import time
from collections import deque


def get_building():
    building_height = random.randint(5, 20)

    building = []
    for floor in range(building_height + 1):
        people = random.randint(0, 10)
        other_floors = [i for i in range(building_height) if i != floor]
        building.append([random.choice(other_floors) for _ in range(people)])

    return building


def load_unload_passengers(elevator, floor, building, destination_floor, moving_up):
    n = len(building)
    m = len(building[floor])
    other_floors = [i for i in range(n) if i != floor]
    unload_passengers = 0

    if floor in elevator:
        for _ in range(elevator.count(floor)):
            elevator.remove(floor)
            building[floor].insert(0, random.choice(other_floors))
            unload_passengers += 1
        for i in building[floor][unload_passengers:]:
            if len(elevator) == 5:
                break
            if moving_up:
                if i > floor:
                    building[floor].remove(i)
                    elevator.append(i)
            else:
                if i < floor:
                    building[floor].remove(i)
                    elevator.append(i)

    elif len(elevator) < 5:
        for _ in range(m):
            if len(elevator) == 5:
                break
            passenger = building[floor].pop()
            if moving_up:
                if passenger > floor:
                    elevator.append(passenger)
                else:
                    building[floor].insert(0, passenger)
            else:
                if passenger < floor:
                    elevator.append(passenger)
                else:
                    building[floor].insert(0, passenger)

    else:
        return elevator, destination_floor

    if elevator:
        if moving_up:
            max_in_elevator = max(elevator)
            if destination_floor is None:
                destination_floor = max_in_elevator
            if max_in_elevator > destination_floor:
                destination_floor = max_in_elevator
        else:
            min_in_elevator = min(elevator)
            if destination_floor is None:
                destination_floor = min_in_elevator
            if min_in_elevator < destination_floor:
                destination_floor = min_in_elevator

    return elevator, destination_floor


def decide_move_up_or_down(floor, building):
    down = 0
    up = 0
    for passenger in building[floor]:
        if passenger > floor:
            up += 1
        else:
            down += 1
    if up == down:
        return bool(random.choice([down, up]))
    return up > down


def show_current_situation(elevator, building, current_floor, count):
    print('{:-^50}'.format(f' stage {count} '))
    print('{:5} | {:^30} | {:2}'.format('floor', 'elevator', 'passengers on each floor'))
    for idx, floor in reversed(list(enumerate(building))):
        passengers = ' '.join(str(i) for i in floor)
        lift = ' '.join(str(i) for i in elevator) if elevator else 'empty'
        if current_floor == idx:
            msg = '{:5} | {:^30} ^| {:2}'
            print(msg.format(idx, lift, passengers))
        else:
            msg = '{:5} | {:^30} | {:2}'
            print(msg.format(idx, '', passengers))
    print('{:*^50}'.format(''))


def did_we_arrive(current_floor, destination_floor, elevator, moving_up):
    if current_floor == destination_floor and len(elevator) == 0:
        if moving_up:
            current_floor -= 1
        else:
            current_floor += 1
        moving_up = None
        destination_floor = None
        return current_floor, moving_up, destination_floor
    return current_floor, moving_up, destination_floor


def run_elevator(building, sleep_time=None):
    elevator = deque([], maxlen=5)
    moving_up = True
    destination_floor = None
    current_floor = 0
    count = 0
    while True:
        if sleep_time:
            time.sleep(sleep_time)
        show_current_situation(elevator, building, current_floor, count)
        if moving_up is None:
            moving_up = decide_move_up_or_down(current_floor, building)
        if moving_up:
            elevator, destination_floor = load_unload_passengers(elevator, current_floor, building, destination_floor, moving_up)
            current_floor, moving_up, destination_floor = did_we_arrive(current_floor, destination_floor, elevator, moving_up)
            current_floor += 1
        else:
            elevator, destination_floor = load_unload_passengers(elevator, current_floor, building, destination_floor, moving_up)
            current_floor, moving_up, destination_floor = did_we_arrive(current_floor, destination_floor, elevator, moving_up)
            current_floor -= 1

        count += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Elevator Simulation')
    parser.add_argument(
        '-d', '--delay', type=int, default=1,
        help='Time delay in seconds.(default=1)'
    )
    args = parser.parse_args()
    building = get_building()
    run_elevator(building, args.delay)
